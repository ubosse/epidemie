#include "mainwindow.h"
#include "settingdialog.h"
#include "welt.h"
#include "datenlogger.h"
#include <QApplication>

int main(int argc, char *argv[])
{
    QApplication a(argc, argv);
    MainWindow * w=new MainWindow;
    w->show();
    w->welt->settingDialog->raise();
    /*Datenlogger*dL=new Datenlogger;
    dL->setXRange(12,1000);
    dL->setYRange(13,10000);
    dL->setGeometry(10,10,1200,410);
    dL->show();*/

    return a.exec();
}
