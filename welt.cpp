#include "welt.h"
#include <cstdlib>
#include <ctime>
#include <QPainter>
#include <QtWidgets>
#include <QtCore>
short Welt::wirtSize=3;
Welt::Welt(MainWindow*mW) : QWidget(mW)
{
    srand (time(NULL));
    setFocusPolicy(Qt::StrongFocus);
    mainWindow=mW;
    infoLabel=new QLabel(mW->statusBar());
    countLabel=new QLabel(mW->statusBar());
    mW->statusBar()->addPermanentWidget(infoLabel);
    mW->statusBar()->addWidget(countLabel);
    speedSlider=new QSlider(Qt::Horizontal);
    speedSlider->setFixedWidth(120);
    speedSlider->setTickInterval(100);
    speedSlider->setTickPosition(QSlider::TicksBelow);
    speedSlider->setRange(1,1000);
    speedSlider->setValue(100);
    speedSlider->setToolTip("simulierter Tag");
    mW->statusBar()->addPermanentWidget(speedSlider);
    connect(speedSlider,SIGNAL(valueChanged(int)),this,SLOT(speedChanged(int)));
    //jetzt den Timer initialisieren.
    timer=new QTimer(this);
    timer->setInterval(100);
    timer->setSingleShot(false);
    connect(timer,SIGNAL(timeout()),this,SLOT(nextDay()));
    QAction*infectAction=new QAction(this);
    infectAction->setShortcut(Qt::Key_I);
    connect(infectAction,SIGNAL(triggered(bool)),this,SLOT(infectByI()));
    addAction(infectAction);
    logger=new Datenlogger;
    QDockWidget*loggerDock=new QDockWidget("Krankenstand",MainWindow::mainWindow);
    loggerDock->setAllowedAreas(Qt::TopDockWidgetArea|Qt::BottomDockWidgetArea);
    loggerDock->setFeatures(loggerDock->features()&~QDockWidget::DockWidgetClosable);
    logger->initialize(0,0,100,50,200,QColor(Wirt::brushes[Wirt::symptomious].color()));
    //logger->setGeometry(0,0,this->width(),200);
    connect(this,SIGNAL(destroyed(QObject*)),logger,SLOT(close()));
    loggerDock->setWidget(logger);
    MainWindow::mainWindow->addDockWidget(Qt::BottomDockWidgetArea,loggerDock);

    logger->show();
    logger->resize(logger->width(),200);
}

void Welt::initWirtliste()
{
    breite=width()/wirtSize;
    hoehe=height()/wirtSize;
    int count= breite*hoehe;
    wirte.clear();
    for(int i=0;i<count;i++)
        wirte.append(Wirt());
    if(settingDialog==nullptr){
        settingDialog=new SettingDialog;
        QRect r=QGuiApplication::primaryScreen()->geometry();
        settingDialog->move(r.width()-settingDialog->width(),100);
        settingDialog->show();
        connect(this,SIGNAL(destroyed(QObject*)),settingDialog,SLOT(close()));
    }
    if(settingDialog->isHidden())
        settingDialog->show();
    for(short i=0;i<7;i++) statistic[i]=0;
    statistic[Wirt::sane]=count;
    repaint();
}

void Welt::changeState(Welt::State astate)
{
    state=astate;//hier wird wohl noch einiges hinzu kommen.
    switch(state){
    case running:
        timer->start();
        mainWindow->statusBar()->showMessage("Simulation gestartet",1000);
        break;
    case pausing:
        mainWindow->statusBar()->showMessage("Simulation angehalten",1000);
        /* fall through */
    case waitForInfect:
        timer->stop();
        break;
    }
}

void Welt::paintEvent(QPaintEvent *event)
{
    QPainter paint(this);
    paint.setBrush(Qt::black);
    paint.drawRect(rect());
    for(int i=0;i<wirte.count();i++){
        //erst entscheiden, ob dieser Wirt überhaupt gezeichnet werden muss, oder ob er überdeckt wird
        if(i>0&&(wirte.at(i-1).state==Wirt::infective || wirte.at(i-1).state==Wirt::symptomious))
            continue;
        if(i>=breite&&(wirte.at(i-breite).state==Wirt::infective||wirte.at(i-breite).state==Wirt::symptomious))
            continue;
        if(i>breite&&(wirte.at(i-breite-1).state==Wirt::infective||wirte.at(i-breite-1).state==Wirt::symptomious))
            continue;
        //jetzt wirt Zeichnen.
        const Wirt & w=wirte.at(i);
       /*paint.setBrush(Wirt::brushes[w.state]);
        paint.drawRect((i%breite)*wirtSize,(i/breite)*wirtSize,wirtSize,wirtSize);
        */
        short dicke;
        if(w.state==Wirt::infective || w.state==Wirt::symptomious){
            dicke=2*wirtSize;
        }else {
            dicke=wirtSize;
        }
        paint.fillRect((i%breite)*wirtSize,(i/breite)*wirtSize,dicke,dicke,Wirt::brushes[w.state]);
    }
}

void Welt::resizeEvent(QResizeEvent *event)
{
    initWirtliste();
    countLabel->setText(QString("%1 Wirte").arg(wirte.count()));
}

void Welt::mousePressEvent(QMouseEvent *event)
{
    if(event->button()==Qt::LeftButton){
        if(state==waitForInfect){
            ((QMainWindow*)parentWidget())->statusBar()->clearMessage();
            int i=event->pos().x()/wirtSize+event->pos().y()/wirtSize*breite;

            if(i<wirte.count()){
                wirte[i].state=Wirt::infected;
                repaint();
            }else{
                QMessageBox::warning(0,"Mist","Mausklick auf nicht vorhandenen Wirt. Seltsam.");
            }
            event->accept();
        }
    }
}

void Welt::speedChanged(int newDelay)
{
    if(timer->isActive()){
        timer->stop();
        timer->setInterval(newDelay);
        timer->start();
    }else{
        timer->setInterval(newDelay);
    }
    infoLabel->setText(QString::fromUtf8("Tag=%1 ms").arg(newDelay));
}
/** Diese Funktion bestimmt für jeden Wirt in der Umgebung eines infizierten, ob er infiziert werden soll
 * oder nicht. Das ist recht langsam bei großen Umgebungen.
void Welt::nextDay()
{
    for(short i=0;i<6;i++)statistic[i]=0;
    statistic[6]+=1;
    for(Wirt&w:wirte){
        w.nextDay();
        statistic[w.state]++;
    }
    for(int i=0;i<wirte.count();i++){
        const Wirt & w=wirte.at(i);
        if (w.state==Wirt::infective || w.state==Wirt::symptomious){
            int x=i%breite;
            int y=i/breite;
            for(int x1=x-Wirt::radius;x1<=x+Wirt::radius;x1++){
                int x11=x1;
                if(x11<0) x11+=breite;
                else if (x11>=breite) x11-=breite;
                for(int y1=y-Wirt::radius;y1<=y+Wirt::radius;y1++){
                    int y11=y1;
                    if(y11<0) y11+=hoehe;
                    else if(y11>=hoehe) y11-=hoehe;
                    Wirt & w2=wirte[x11+y11*breite];//w2 ist der Wirt, der angesteckt wird.
                    if(w2.state==Wirt::sane){
                        if(Wirt::zuf01()<Wirt::infectionRate)
                            w2.state=Wirt::infected;
                    }

                }
            }
        }//Wirt ist infective
    }//for-Schleife durch alle Wirte
    repaint();
    infoLabel->setText(QString::fromUtf8("Tag %7: gesund %1, infiziert %2, infektiös %3, krank %4, immun %5, tot %6").
                       arg(statistic[0]).arg(statistic[1]).arg(statistic[2]).arg(statistic[3]).arg(statistic[4]).arg(statistic[5]).
                        arg(statistic[6]));
    if(mainWindow->logAction->isChecked()){
        mainWindow->logStream <<statistic[6]<<";"<< statistic[0]<<";"<<statistic[1]<<";"<<statistic[2]<<";"<<statistic[3]<<";"<<statistic[4]<<";"
                                            <<statistic[5]<<";"<<"\n";
    }
    if(statistic[Wirt::infected]+statistic[Wirt::infective]+statistic[Wirt::symptomious]==0){
        changeState(pausing);
        QMessageBox::information(0,"Ende!","Seuche breitet sich nicht mehr aus.");
        if(mainWindow->logAction->isChecked()){
            mainWindow->logAction->setChecked(false);
            mainWindow->logger();
        }
    }
}**/
/** Diese Funktion bestimmt erst die Anzahl der zu infizierenden Wirte und wählt dann soviele aus der Umgebung
 * zufällig aus **/
void Welt::nextDay()
{
    static int gestern;//kumulative Anzahl der Infizierten.
    for(short i=0;i<6;i++)statistic[i]=0;
    statistic[6]+=1;
    for(Wirt&w:wirte){//jeder Wirt altert um einen Tag und ändert entsprechend seinen Status
        w.nextDay();
        statistic[w.state]++;
    }
    for(int i=0;i<wirte.count();i++){//hier muss die Ansteckung realisiert werden.
        const Wirt & w=wirte.at(i);//Betrachtet wird w...
        if (w.state==Wirt::infective || w.state==Wirt::symptomious){//...ist w ansteckend?
            int x=i%breite;
            int y=i/breite;//x,y: Koordinaten von w. Wie viele steckt w aus seiner Umgebung an?
            int k=Wirt::binompdf(Wirt::randomGenerator);//binompdf mit n=#in Umgebung p=Infektionsrate.
            for(short i=0;i<k;i++){
                uint j=Wirt::zuf01()*Wirt::umgebung;//durch Abrundung ist Ergebnis zwischen 0 und umgebung-1;
                /** kleines Manko: ist k>1, dann könnte zweimal der gleiche gewählt werden **/
                if(j>=Wirt::umgebung/2) j++;//j=umgebung/2 würde auf w verweisen!.
                //jetzt koordinaten im Umgebungsquadrat ermitteln.(0,0): links oben, (r,r)=Wirt w.
                int x1=j%(2*Wirt::radius+1); int y1=j/(2*Wirt::radius+1);
                //jetzt die globalen Wirt-Koordinaten in der Welt bestimmen.
                x1=x1-Wirt::radius+x; y1=y1-Wirt::radius+y;
                if(x1<0) x1+=breite;
                else if(x1>=breite) x1-=breite;
                if(y1<0) y1+=hoehe;
                else if(y1>=hoehe) y1-=hoehe;
                int toInfect=y1*breite+x1;//=Index des zu infizierenden Wirtes
                wirte[toInfect].infect();
            }
        }//Wirt ist infective
    }//for-Schleife durch alle Wirte
    repaint();
    infoLabel->setText(QString::fromUtf8("Tag %7: gesund %1, infiziert %2, infektiös %3, krank %4, immun %5, tot %6").
                       arg(statistic[Wirt::sane]).arg(statistic[Wirt::preinfective]).arg(statistic[Wirt::infected]).
                        arg(statistic[Wirt::symptomious]).arg(statistic[Wirt::imune]).arg(statistic[Wirt::dead]).
                        arg(statistic[6]));
    if(mainWindow->logAction->isChecked()){
        if(statistic[6]==1){
            gestern=0;
            Wirt::writeParameter(mainWindow->logStream);
            mainWindow->logStream<<QString::fromUtf8("Gesamtbevölkerung;")<<wirte.count()<<"\n";
            mainWindow->logStream<<"Tag;gesund;infiziert;ansteckend;krank;immun;tot;Neuinfektionen\n";
        }
        int kumul=statistic[Wirt::infective]+statistic[Wirt::infected]+statistic[Wirt::symptomious]+
                statistic[Wirt::imune]+statistic[Wirt::dead];
        int neu=kumul-gestern; gestern=kumul;
        mainWindow->logStream <<statistic[6]<<";"<< statistic[0]<<";"<<statistic[1]<<";"<<statistic[2]<<";"<<statistic[3]<<";"
                                           <<statistic[4]<<";"<<statistic[5]<<";"<<neu<<"\n";
    }
    if(statistic[Wirt::infected]+statistic[Wirt::infective]+statistic[Wirt::symptomious]==0){
        changeState(pausing);
        QMessageBox::information(0,"Ende!","Seuche breitet sich nicht mehr aus.");
        if(mainWindow->logAction->isChecked()){
            mainWindow->logAction->setChecked(false);
            mainWindow->logger();
        }
    }
    logger->addPoint(QPoint(statistic[6],statistic[Wirt::symptomious]));
}

void Welt::infectWirt()
{
    QMainWindow * mW=(QMainWindow*)parentWidget();
    mW->statusBar()->showMessage(QString::fromUtf8("Mit linker Maustaste zu infizierenden Wirt anklicken"));
    changeState(waitForInfect);

}
void Welt::infectByI()
{
    QPoint p=mapFromGlobal(QCursor::pos());
    int i=(p.x()/wirtSize)%breite + (p.y()/wirtSize)*breite;
    if(i>=0&&i<wirte.count()){
        wirte[i].setState(Wirt::infected);
        repaint();
    }
}

void Welt::startStop()
{
    switch(state){
    case waitForInfect:
    case pausing:
        changeState(running);
        break;
    case running:
        changeState(pausing);
    }
}

void Welt::reset()
{
    for(Wirt&w:wirte){
        w.reset();
    }
    statistic[6]=0;
    repaint();
}

void Welt::resetWithImmunity()
{
    immunity=QInputDialog::getDouble(0,"Herdenimmunitätsquotient","Anteil der durch Immunität geschützten Wirte",immunity,0.0,1.0,4);
    for (Wirt&w:wirte){
        w.reset();
        if(Wirt::zuf01()<immunity)
            w.setState(Wirt::imune);
    }

}

void Welt::changeSize()
{
    QDialog * dial=new QDialog;
    QFormLayout * lay=new QFormLayout(dial);
    QLineEdit * sizeX=new QLineEdit(QString::number(breite),dial);
    sizeX->setInputMask("9000");
    lay->addRow("Breite (Wirte)",sizeX);
    QLineEdit * sizeY=new QLineEdit(QString::number(hoehe),dial);
    sizeY->setInputMask("9000");
    lay->addRow("Höhe (Wirte)",sizeY);
    QLineEdit * sizeWirt =new QLineEdit(QString::number(wirtSize),dial);
    sizeWirt->setInputMask("9");
    lay->addRow(QString::fromUtf8("Größe der Wirte (Pixel)"),sizeWirt);
    QDialogButtonBox * bb=new QDialogButtonBox(QDialogButtonBox::Ok|QDialogButtonBox::Cancel,dial);
    connect(bb, &QDialogButtonBox::accepted, dial, &QDialog::accept);
    connect(bb, &QDialogButtonBox::rejected, dial, &QDialog::reject);
    lay->addRow(bb);
    dial->setLayout(lay);
    if(dial->exec()==QDialog::Accepted){
        wirtSize=sizeWirt->text().toInt();
        setGeometry(pos().x(),pos().y(),wirtSize*sizeX->text().toInt(),wirtSize*sizeY->text().toInt());
        setSizePolicy(QSizePolicy::Fixed,QSizePolicy::Fixed);
        //mainWindow->adjustSize();
    }
    delete dial;
}

void Welt::showSettings()
{
    if(settingDialog!=nullptr){
        if(settingDialog->isVisible())
            settingDialog->raise();
        else
            settingDialog->show();
    }
}
