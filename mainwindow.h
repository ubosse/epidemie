#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>
#include <QTextStream>
class Welt;
namespace Ui {
class MainWindow;
}

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    static MainWindow*mainWindow;
    explicit MainWindow(QWidget *parent = 0);
    ~MainWindow();
    Welt * welt=nullptr;
    QAction*logAction=nullptr;
    QTextStream logStream;
private:
    Ui::MainWindow *ui;
public slots:
    void logger();
};

#endif // MAINWINDOW_H
