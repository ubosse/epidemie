#ifndef WELT_H
#define WELT_H
#include "wirt.h"
#include "settingdialog.h"
#include "mainwindow.h"
#include "datenlogger.h"
#include <QtWidgets>
#include <QTimer>
typedef QList<Wirt> Wirtliste;
class Welt : public QWidget
{
    Q_OBJECT
public:
    enum State{pausing,running,waitForInfect};
    State state=pausing;
    explicit Welt(MainWindow * mW = nullptr);
    void initWirtliste();//initialisiert die Wirtliste, deren Größe an die Größe des Widgets angepasst ist. Also muss die zuerst stimmen.
    void changeState(State astate);
    Wirtliste wirte;
    SettingDialog* settingDialog=nullptr;
    int statistic[7];
private:
    static short wirtSize;//ein Wirt braucht wirtSize x wirtSize viele Pixel in der Darstellung
    int breite,hoehe;//breite x hoehe viele Wirte hat die Welt, (width{}/wirtSize, height()/wirtSize;
    MainWindow * mainWindow;
    virtual void paintEvent(QPaintEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
    virtual void mousePressEvent(QMouseEvent *event);
    QLabel * infoLabel;//kann in der statusBar infosAnzeigen.
    QLabel * countLabel;//zeigt in StatusBar die Gesamtzahl aller Wirte.
    QSlider * speedSlider;//zum Einstellen der Simulationsgeschwindigkeit.
    QTimer * timer;
    Datenlogger*logger=nullptr;
    double immunity=0.0;
 private slots:
    void speedChanged(int newDelay);//pause zwischen zwei Simulationsschritten.
    void infectByI();//infiziert den Wirt unter der Maus beim Drücken der Taste I.
 public slots:
    void nextDay();//die Wirte in der wirtliste werden einen Tag älter und stecken sich an...
    void infectWirt();//durch Mausklick wird ein Wirt infiziert.
    void startStop();
    void reset();
    void resetWithImmunity();
    void changeSize();//ruft Dialog zur Größenänderung auf.
    void showSettings();


};

#endif // WELT_H
