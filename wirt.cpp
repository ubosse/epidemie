#include "wirt.h"
#include <cstdlib>
#include <cmath>
#include <random>
#include <QTextStream>
std::default_random_engine Wirt::randomGenerator;
double Wirt::zuf01(){
    return 1.0*rand()/RAND_MAX;
}

short Wirt::infiziereSoviel()
{
    return binompdf(randomGenerator);//binomialverteilung mit n=#in Umgebung, p=infektionsRate;
}

void Wirt::writeParameter(QTextStream &s)
{
    s<<QString::fromUtf8("R;bisAnsteckend;bisKrank;bisGesund;Mortalität;Radius\n");
    s<<infectionFactor<<";"<<preinfective<<";"<<presymptomious<<";"<<sicktime<<";"<<mortality<<";"<<radius<<"\n";
}
/** Initialisierung statischer Variablen *************************************************************************/
uint Wirt::preinfective=3; //Zeit von der Ansteckung bis man selbstansteckend ist.
uint Wirt::presymptomious=5; //Zeit bis man Symptome zeigt
uint Wirt::sicktime=12; //Zeit, bis zur Immunität, danach: gesund oder Tot.
short Wirt::radius=10; //die Wirte in diesem Radius können angesteckt werden (es befinden sich rund radius^2 viele Wirte darin.
uint Wirt::umgebung=440;
double Wirt::infectionFactor=3.0; //soviele sane Wirte aus der Umgebung werden in der Zeit, in der der ein Wirt ansteckend ist, angesteckt.
double Wirt::infectionRate=3.0/440/(12-3);// 3/440/(12-3).
double Wirt::mortality=0.006;//Tödlichkeit
std::binomial_distribution<int> Wirt::binompdf(440,Wirt::infectionRate);
QBrush Wirt::brushes[6]={QBrush(Qt::blue),QBrush(Qt::yellow),QBrush(QColor("orange")),QBrush(Qt::red),
                         QBrush(Qt::darkGreen),QBrush(Qt::black)};

void Wirt::nextDay()
{
    if(state==sane || state==imune)return;
    daysSinceInfection++;
    switch (state){
    case infected :
        if (daysSinceInfection>preinfective)
            state=infective;
        break;
    case infective :
        if(daysSinceInfection>presymptomious)
            state=symptomious;
        break;
    case symptomious:
        if(daysSinceInfection>sicktime){
            if(zuf01()<mortality)
                state=dead;
            else
                state=imune;
        }
    default:    break;
    }
    return;
}

void Wirt::infect()
{
    if(state==sane)
        state=infected;
}

Wirt::State Wirt::getState() const
{
    return state;
}

void Wirt::setState(const State &value)
{
    state = value;
}

void Wirt::reset()
{
    daysSinceInfection=0;
    state=sane;
}

