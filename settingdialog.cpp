#include "settingdialog.h"
#include "ui_settingdialog.h"
#include "wirt.h"

SettingDialog::SettingDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingDialog)
{
    ui->setupUi(this);
    ui->preinfective->setValue(Wirt::preinfective);
    ui->presymptomious->setValue(Wirt::presymptomious);
    ui->sicktime->setValue(Wirt::sicktime);
    ui->infectionFactor->setText(QString::number(Wirt::infectionFactor));
    ui->mortality->setText(QString::number(Wirt::mortality));
    ui->radius->setValue(Wirt::radius);
    setWindowTitle(QString::fromUtf8("Parameter der Seuchenwelt"));
    connect(qApp,SIGNAL(aboutToQuit()),this,SLOT(close()));
}

SettingDialog::~SettingDialog()
{
    delete ui;
}

void SettingDialog::on_okButton_clicked()
{
    Wirt::preinfective=ui->preinfective->value();
    Wirt::presymptomious=ui->presymptomious->value();
    Wirt::sicktime=ui->sicktime->value();
    Wirt::infectionFactor=ui->infectionFactor->text().toDouble();
    Wirt::radius=ui->radius->value();
    Wirt::umgebung=4*Wirt::radius*(Wirt::radius+1);
    Wirt::infectionRate=Wirt::infectionFactor/Wirt::umgebung/(Wirt::sicktime-Wirt::preinfective);
    Wirt::mortality=ui->mortality->text().toDouble();
    Wirt::binompdf=std::binomial_distribution<int>(Wirt::umgebung,Wirt::infectionRate);
    //hier eventuell Werte in Settings abspeichern.
}

void SettingDialog::on_cancelButton_clicked()
{
    ui->preinfective->setValue(Wirt::preinfective);
    ui->presymptomious->setValue(Wirt::presymptomious);
    ui->sicktime->setValue(Wirt::sicktime);
    ui->infectionFactor->setText(QString::number(Wirt::infectionFactor));
    ui->mortality->setText(QString::number(Wirt::mortality));
    ui->radius->setValue(Wirt::radius);
}
