#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "welt.h"
#include <QtCore>
MainWindow * MainWindow::mainWindow=nullptr;
MainWindow::MainWindow(QWidget *parent) :
    QMainWindow(parent),
    ui(new Ui::MainWindow)
{
    ui->setupUi(this);
    setAttribute(Qt::WA_DeleteOnClose);
    mainWindow=this;
    welt=new Welt(this);
    welt->setGeometry(0,0,800,800);
    welt->initWirtliste();
    //QRect screenrect=QApplication::desktop()->screenGeometry();
    //setGeometry(0,0,screenrect.width()-200,screenrect.height());
    setGeometry(0,0,810,890);
    QScrollArea * scroll=new QScrollArea(this);
    scroll->setBackgroundRole(QPalette::Dark);
    scroll->setAlignment(Qt::AlignHCenter|Qt::AlignVCenter);
    scroll->setWidget(welt);
    setCentralWidget(scroll);
    //welt->initWirtliste();
    logAction= ui->actionVerlauf_mitloggen;
    connect(ui->actionInfizieren,SIGNAL(triggered(bool)),welt,SLOT(infectWirt()));
    connect(ui->actionStartStop,SIGNAL(triggered(bool)),welt,SLOT(startStop()));
    connect(ui->actionReset,SIGNAL(triggered(bool)),welt,SLOT(reset()));
    connect(ui->actionResetImmun,SIGNAL(triggered(bool)),welt,SLOT(resetWithImmunity()));
    connect(ui->actionParameterfenster,SIGNAL(triggered(bool)),welt,SLOT(showSettings()));
    connect(ui->actionVerlauf_mitloggen,SIGNAL(triggered(bool)),this,SLOT(logger()));
    connect(ui->actionQuit,SIGNAL(triggered(bool)),this,SLOT(close()));
    connect(ui->actionWeltGroesse,SIGNAL(triggered(bool)),welt,SLOT(changeSize()));

}

MainWindow::~MainWindow()
{
    delete ui;
}

void MainWindow::logger()
{
    if(logAction->isChecked()){
        QFile* logfile=new QFile("/tmp/seuche.csv");
        logfile->open(QIODevice::WriteOnly);
        logStream.setDevice(logfile);
    }else{
        if(logStream.device()!=0){
            QString fname=((QFile*)(logStream.device()))->fileName();
            logStream.device()->close();
            delete logStream.device();
            QMessageBox::information(0,"Info",QString::fromUtf8("Logdatei %1 geschlossen.").arg(fname));
            logStream.setDevice(0);
        }
    }
}
