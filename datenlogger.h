#ifndef DATENLOGGER_H
#define DATENLOGGER_H

#include <QWidget>
#include <QList>
#include <QTimer>
#include <QtCore>
typedef QList<QPoint> Pointlist;
struct DataRange {
    int xmin; int xmax; int ymin; int ymax;
};
class Datenlogger : public QWidget
{
    Q_OBJECT
public:
    explicit Datenlogger(QWidget *parent = nullptr);
    void initialize(int axmin, int aymin, int axwidth, int aywidth, int interval,QColor col);
    QColor getColor() const;
    void setColor(const QColor &value);
    virtual void paintEvent(QPaintEvent *event);
    QTimer paintTimer;

signals:

public slots:
    void addPoint(QPoint p);
    void clear();//löscht die pointlist
    void setXRange(int min,int width){xmin=min;xwidth=width;}
    void setYRange(int min,int width){ymin=min;ywidth=width;}//setzt die Zeichenbereiche
    virtual void closeEvent(QCloseEvent *event);
    virtual void resizeEvent(QResizeEvent *event);
private slots:
    void actualize();
private:
    Pointlist points;
    int xmin,ymin,xwidth,ywidth;//Bereiche der Achsen
    DataRange datarange={0,0,0,0};
    QColor color;//farbe der Punkte
    int count=0;//zählt die gezeichneten Punkte, um festzustellen, ob neues zu zeichnen ist.
    bool autoZoom=true;//passt automatisch den Zeichenbereich an die Daten an.
};

#endif // DATENLOGGER_H
