#ifndef WIRT_H
#define WIRT_H
#include <QBrush>
#include <random>
typedef unsigned int uint;
class QTextStream;
class Wirt
{
public:
    static double zuf01();
    static std::default_random_engine randomGenerator;
    static std::binomial_distribution<int> binompdf;
    static QBrush brushes[6];
    enum  State{sane,infected,infective,symptomious,imune,dead};
    static uint preinfective; //Zeit von der Ansteckung bis man selbstansteckend ist.
    static uint presymptomious; //Zeit bis man Symptome zeigt
    static uint sicktime; //Zeit, bis zur Immunität, danach: gesund oder Tot.
    static short radius; //die Wirte in diesem Radius können angesteckt werden (es befinden sich rund radius^2 viele Wirte darin.
    static uint umgebung;//=(2*radius+1)^2-1=4*(radius*radius+radius)
    static double infectionFactor; //soviele sane Wirte aus der Umgebung werden in der Zeit, in der der ein Wirt ansteckend ist, angesteckt
    static double infectionRate; //=Factor/(radius^2*anzahl Tage, die man ansteckend ist.
    static double mortality;//Tödlichkeit
    static short infiziereSoviel();// Berechnet aus den Daten, wie viele pro Tag aus der Umgebung zu infizieren sind.
        //das wird zufällig entschieden gemäß Binomialverteilung
    static void writeParameter(QTextStream & s);
    State state=sane;
    uint daysSinceInfection=0;
    void nextDay();//ändert gemäß der Parameter den Zustand
    void infect();//der Wirt wird infiziert.
    State getState() const;
    void setState(const State &value);
    void reset();
};
#endif // WIRT_H
