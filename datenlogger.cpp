#include "datenlogger.h"
#include <QtCore>
#include <QtWidgets>
Datenlogger::Datenlogger(QWidget *parent) : QWidget(parent)
{
    connect(&paintTimer,SIGNAL(timeout()),this,SLOT(actualize()));
    setWindowTitle("Verlauf des Krankenstandes.");
    QToolButton * but=new QToolButton(this);
    but->setObjectName("clearBut");
    but->setIcon(QIcon::fromTheme("edit-clear"));
    but->move(width()-but->width()-2,2);
    but->setToolTip(QString::fromUtf8("Kurven löschen"));
    but->show();
    but->clearFocus();
    but->setFocusPolicy(Qt::NoFocus);
    connect(but,SIGNAL(clicked(bool)),this,SLOT(clear()));
}

void Datenlogger::initialize(int axmin, int aymin, int axwidth, int aywidth, int interval, QColor col)
{
    xmin=axmin;ymin=aymin;xwidth=axwidth;ywidth=aywidth;
    color=col;
    paintTimer.setInterval(interval);
    paintTimer.start();
}

QColor Datenlogger::getColor() const
{
    return color;
}

void Datenlogger::setColor(const QColor &value)
{
    color = value;
}

void Datenlogger::paintEvent(QPaintEvent *event)
{
    QPainter paint(this);
    //paint.setBrush(Qt::white);
    QRect r=rect();
    QMargins margin(paint.pen().width(),paint.pen().width(),paint.pen().width(),paint.pen().width());
    r-=margin;
    paint.drawRect(r);
    paint.drawText(r,Qt::AlignLeft|Qt::AlignTop,QString::number(ymin+ywidth));
    paint.drawText(r,Qt::AlignRight|Qt::AlignBottom,QString::number(xmin+xwidth));
    paint.setPen(QPen(color));
    for (QPoint & p : points){
        int x = (p.x()-xmin)*width()/xwidth;
        int y = height()-(p.y()-ymin)*height()/ywidth;
        paint.drawLine(x-2,y,x+2,y);
        paint.drawLine(x,y-2,x,y+2);
    }
    count=points.count();
    event->accept();
}

void Datenlogger::addPoint(QPoint p)
{
    points.append(p);
    if(p.x()>datarange.xmax)datarange.xmax=p.x();
    if(p.y()>datarange.ymax) datarange.ymax=p.y();
}

void Datenlogger::clear()
{
   points.clear();
   count=0;
   xmin=0;xwidth=300;ymin=0;ywidth=400;
   datarange.xmax=datarange.ymax=0;
   repaint();
}

void Datenlogger::closeEvent(QCloseEvent *event)
{
    if(event->spontaneous()){
        if(QMessageBox::question(0,"Frage",QString::fromUtf8("Soll das Fenster wirklich geschlossen werden?"
                                 " es kann nicht wieder geöffnet werden"))==QMessageBox::No)
            event->ignore();
    }
}

void Datenlogger::resizeEvent(QResizeEvent *event)
{
    QWidget* clearBut=this->findChild<QWidget*>(QString("clearBut"));
    clearBut->move(width()-2-clearBut->width(),2);
}

void Datenlogger::actualize()
{
    if(points.count()==count)
        return;
    if(autoZoom){
        if(datarange.xmax>xmin+xwidth){
            xwidth=(datarange.xmax-xmin)*4/3;
        }
        if(datarange.ymax>ymin+ywidth){
            ywidth=(datarange.ymax-ymin)*4/3;
        }
    }
    repaint();
}
